package com.applista.qa.helpers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import com.applista.qa.utilities.ReadConfig;

public class BaseClass {
	
	public static ReadConfig readconfig =new ReadConfig();
	public static WebDriver driver;
	public static String baseURL = readconfig.getapplicationURL();
	public static String browser = readconfig.browser();
	
public static WebDriver Setup() throws MalformedURLException {
	
	 DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	  URL url = new URL("http://localhost:4444/wd/hub");
	  RemoteWebDriver driver = new RemoteWebDriver(url,capabilities);
	  
	PropertyConfigurator.configure("./src/main/resources/Log/log4j.properties");
//try{
//	if(browser.equalsIgnoreCase("chrome")) {
//	System.setProperty("webdriver.chrome.driver",readconfig.Chromepath());}
//	driver = new ChromeDriver();}
//	Log.info("Chrome driver initiated");}

/*else if(browser.equalsIgnoreCase("Firefox")){
	System.setProperty("webdriver.gecko.driver", "readconfig.Mozillapath()");
	driver = new FirefoxDriver();}
//	Log.info("Frefox driver initiated");}
	
else if(browser.equalsIgnoreCase("IE")){
	System.setProperty("webdriver.ie.driver",readconfig.iepath());
	driver = new InternetExplorerDriver();}
//	Log.info("Internet Explorer driver initiated");}*/
//}	
//catch (Exception e) {
//	System.out.println("Exception is "+e.getMessage());}
//
//	driver.manage().window().maximize();
//	driver.manage().deleteAllCookies();
	
//try {
	driver.get(baseURL);
	driver.manage().timeouts().pageLoadTimeout(3000, TimeUnit.SECONDS);	
	Assert.assertEquals("LOGIN", "LOGIN");
	
//}catch(Exception e) {
//	System.out.println("Exception is "+e.getMessage());}
//	
return driver;
	}
}

