package com.applista.qa.utilities;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class readexcelConfig {

	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;
	
public readexcelConfig(){
	
	File file = new File("./src/test/resources/exceltestdata/testdata.xlsx");
	
  try{
	FileInputStream fis = new  FileInputStream(file);
	workbook = new XSSFWorkbook(fis);}

  catch(Exception e){
		System.out.println(e.getMessage());}
  }

// returns the row count in a sheet
public int getRowCount(String sheetName){ 	    			 
	int index = workbook.getSheetIndex(sheetName);// we can use direct string index statement skipping sheet name
    sheet =workbook.getSheetAt(index);
    int rowcount = sheet.getLastRowNum();// or   int rowcount = sheet.getLastRowNum()+1;
	rowcount=rowcount+1;
	return rowcount ;}


//returns the row count in a sheet
public String getCellData(String sheetName,int row,int column){ 
	int index = workbook.getSheetIndex(sheetName);
	sheet = workbook.getSheetAt(index);  			 
	String data = sheet.getRow(row).getCell(column).getStringCellValue();
	return data ;
}
}