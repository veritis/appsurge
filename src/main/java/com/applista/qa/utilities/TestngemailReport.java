package com.applista.qa.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;

public class TestngemailReport {

@Test
	public static void DailyChecklistTestNGReport() {

		// Create object of Property file
		Properties props = new Properties();

		// this will set host of server- you can change based on your requirement 
		props.put("mail.smtp.host", "smtp.gmail.com");

		// set the port of socket factory 
		props.put("mail.smtp.socketFactory.port", "465");

		// set socket factory
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");

		// set the authentication to true
		props.put("mail.smtp.auth", "true");

		// set the port of SMTP server
		props.put("mail.smtp.port", "465");

		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props,

				new javax.mail.Authenticator() {

					protected PasswordAuthentication getPasswordAuthentication() {

					return new PasswordAuthentication("fitbasetrainerweb@gmail.com", "Testing@123");

					}

				});

		try {

			// Create object of MimeMessage class
			Message message = new MimeMessage(session);

			// Set the from address
			message.setFrom(new InternetAddress("sunilkumartesingfitbase@gmail.com"));

			// Set the recipient address
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("vijaykumar.kalpagur@veritis.com"));
            
                        // Add the subject link
			message.setSubject("Applista_Web_Daily_Checklist TestNG & Extent Report");

			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();

			// Set the body of email
			messageBodyPart1.setText("TestNG Report and Extent Report can be viewable only after downloading file in your machine");

			// Mention the file which you want to send
			List<String> filename = new ArrayList<String>();
					
			
//					filename.add("C:\\Users\\Vijay\\eclipse-workspace\\Applista\\test-output\\emailable-report.html");
//					filename.add("C:\\Users\\Vijay\\eclipse-workspace\\Applista\\test-output\\Extent.html");
					filename.add("C:\\Users\\Vijay\\Desktop\\reports\\CUsersTestingDesktopreportsFitbaseTrainer.html");


				
					// Create object of MimeMultipart class
					Multipart multipart = new MimeMultipart();
					
					// add body part 2
					multipart.addBodyPart(messageBodyPart1);
					
			for(String file : filename){
				
				// Create another object to add another content
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			
			// Create data source and pass the filename
			DataSource source = new FileDataSource(file);

			// set the handler
			messageBodyPart2.setDataHandler(new DataHandler(source));

			// set the file
			messageBodyPart2.setFileName(file);
		
			// add body part 1
			multipart.addBodyPart(messageBodyPart2);

			// set the content
			message.setContent(multipart);

			// finally send the email
			
			}
			System.out.println("======================TestNG Report Sent to Email===============");
			
			Transport.send(message);
		} catch (MessagingException e) {

			throw new RuntimeException(e);
		}
	}
}

