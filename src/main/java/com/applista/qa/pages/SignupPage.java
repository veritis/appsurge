package com.applista.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignupPage {
	
	public SignupPage (WebDriver driver) {
	PageFactory.initElements(driver, this);}   
	
//--------------------------------------------------------------------------------------
//++++++++++++++++++++++++++++++++++ # SIGN UP # +++++++++++++++++++++++++++++++++++++++
//-------------------------------------------------------------------------------------- 

//Valid XPath
	@FindBy(how=How.XPATH, using ="//input[@id ='firstName']")
	public WebElement SignupvalidFirstName;
			
	@FindBy(how=How.XPATH, using ="//input[@id ='lastName']")
	public WebElement SignupvalidLastName;
			
	@FindBy(how=How.XPATH, using ="//input[@id ='email']")
	public WebElement SignupvalidEmailAddress;
			
	@FindBy(how=How.XPATH, using = "//input[@id ='password']" )
	public WebElement SignupvalidPassword;
			
	@FindBy(how=How.XPATH, using = "//button[@id ='submit']" )
	public WebElement SignupContinueButton;
			
//Invalid XPath	
	@FindBy(how=How.XPATH, using = "//div[@id='register-firstname-error']" )
	public WebElement SignupInvalidFirstNameValidation;
			
	@FindBy(how=How.XPATH, using = "//div[@id='register-lastname-error']" )
	public WebElement SignupInvalidLastNameValidation;
			
	@FindBy(how=How.XPATH, using = "//div[@id='register-email-error']" )
	public WebElement SignupInvalidEmailAddressValidation;
			
	@FindBy(how=How.XPATH, using = "//div[@id='register-password-error']" )
	public WebElement SignupInvalidPasswordValidation;
			
// ---------------------------------# EYE ICON #-----------------------------------
			
	@FindBy(how=How.XPATH, using ="//i[@id='passwordicon']")
	public WebElement SignupEyeIcon;
	
	@FindBy(how=How.XPATH, using ="//i[@id='passwordicon' and contains(text(),'visibility_off')]")
	public WebElement SignupEyeIconVisibilityOff;
	
	@FindBy(how=How.XPATH, using ="//i[@id='passwordicon' and contains(text(),'visibility')]")
	public WebElement SignupEyeIconVisibilityOn;
	
	
	
// ----------------------------# SIGNUP PAGE TEXT ,FIELD AND LINKS #-------------------------------
			
	@FindBy(how=How.XPATH, using ="/html/body/div/div/div[2]/div/div/div/div/h2")
	public WebElement SignupHeaderText;
				
	@FindBy(how=How.XPATH, using = "/html/body/div/div/div[2]/div/div/div/div/p")
	public WebElement SignupAlreadyhaveanaccountText;
			
	@FindBy(how=How.XPATH, using = "//p[@class='small']/a")
	public WebElement SignupLoginLink;
			 	
//Signup FirstName Text & Field Display	
	@FindBy(how=How.XPATH, using = "//*[@id=\"signupForm\"]/div[1]/div[1]/div/label")
	public WebElement SignupFirstNameText;
			
	@FindBy(how=How.XPATH, using ="//input[@id ='firstName']")
	public WebElement SignupFirstNameField;
			
//Signup LastName Text & Field Display	
	@FindBy(how=How.XPATH, using = "//*[@id=\"signupForm\"]/div[1]/div[2]/div/label")
	public WebElement SignupLastNameText;
			
	@FindBy(how=How.XPATH, using ="//input[@id ='lastName']")
	public WebElement SignupLastNameField;
			
//Signup Email Text & Field Display				
	@FindBy(how=How.XPATH, using = "//*[@id=\"signupForm\"]/div[2]/label")
	public WebElement SignupEmailAddressText;
			
	@FindBy(how=How.XPATH, using ="//input[@id ='email']")
	public WebElement SignupEmailAddressField;
			
//Signup Password Text & Field Display		
	@FindBy(how=How.XPATH, using = "//*[@id=\"signupForm\"]/div[3]/label")
	public WebElement SignupPasswordText;

	@FindBy(how=How.XPATH, using = "//input[@id ='password']" )
	public WebElement SignupPasswordField;
			
// ------------------------# LOGOUT #-----------------------------------	
			
	@FindBy(how=How.XPATH, using ="//li[@class='nav-item dropdown user-d']//a")
	public WebElement LogoutClickonUserProfile;
		
	@FindBy(how=How.XPATH, using = "//ul[@class='dropdown-menu animate slideIn show']/li[2]/a" )
	public WebElement ClickonLogout;	
	
}

