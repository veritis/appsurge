package com.applista.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OrganizationPage {
	
public OrganizationPage (WebDriver driver) {
	PageFactory.initElements(driver, this);}	

// ------------------------# ORGANIZATION #-----------------------------------	
//  ORGANIZATION TEXT & DISPLAY

//  Signup User Name Text Display		
	@FindBy(how=How.XPATH, using = "/html/body/div/div/div[2]/div/div/div/div/h2")
	public WebElement SignupUserNameText;
	
//  Organization info message Text Display		
	@FindBy(how=How.XPATH, using = "/html/body/div/div/div[2]/div/div/div/div/p")
	public WebElement OrganizationInfoMessageText;
	
//  Applista logo icon Display		
	@FindBy(how=How.XPATH, using = "/html/body/div/div/div[1]/div/a/img")
	public WebElement ApplistaLogoIcon;

//Organization Name Text & Field Display		
	@FindBy(how=How.XPATH, using = "//*[@id='organizationForm']/div[1]/label")
	public WebElement OrganizationNameText;
				
	@FindBy(how=How.XPATH, using ="//Input[@id='name']")
	public WebElement OrganizationNameField;
				
//Organization Website URL Text & Field Display
	@FindBy(how=How.XPATH, using = "//*[@id='organizationForm']/div[2]/label")
	public WebElement OrganizationWebsiteURLText;
				
	@FindBy(how=How.XPATH, using ="//Input[@id='websiteURL' and @placeholder ='www.appsurge.com']")
	public WebElement OrganizationWebsiteURLFieldandPlaceHolder;
							
//Organization You're Done Button Display		
	@FindBy(how=How.XPATH, using = "//button[@type='submit']" )
	public WebElement OrganizationYoureDoneButton;	
		
//Organization	Validations for invalid data
	@FindBy(how=How.XPATH, using = "//div[@id='register-password-error']" )
	public WebElement OrganizationInvalidNameValidationText;	
		
	@FindBy(how=How.XPATH, using = "//div[contains(text(),'Please enter valid website url.')]" )
	public WebElement OrganizationInvalidWebsiteURLValidationText;
	
	

	
	
	
	
	
	
	
}
