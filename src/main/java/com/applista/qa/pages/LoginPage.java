package com.applista.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
public LoginPage (WebDriver driver) {
	PageFactory.initElements(driver, this);}

//Valid XPath
		@FindBy(how=How.XPATH, using ="//input[@id ='username']")
		public WebElement LoginvalidEmailAddress;
	
		@FindBy(how=How.XPATH, using = "//input[@id = 'password']" )
		public WebElement LoginvalidPassword;
	
//Invalid Xpath
		@FindBy(how=How.XPATH, using ="//input[@id ='username']")
		public WebElement LoginInvalidEmailAddress;
		
		@FindBy(how=How.XPATH, using = "//input[@id = 'password']" )
		public WebElement LoginInvalidPassword;
	
//Single Validation for both Email&Password
		@FindBy(how=How.XPATH, using ="//div[@class='is-invalid']//span[@id='error-msg']")
		public WebElement LoginInvalidEmailPasswordValidationMessage;
	
		@FindBy(how=How.XPATH, using ="//div[@class='is-invalid']/span[1]")
		public WebElement LoginInvalidEmail_PasswordValidationMessage;
	
		@FindBy(how=How.XPATH, using ="//div[@id='password-error']")
		public WebElement LoginInvalidPasswordValidationMessage;
	
		@FindBy(how=How.XPATH, using = "//button[@id='submit']")
		public WebElement LoginButton;
	
	
// ------------------------# FAVICON #-----------------------------------
	
		@FindBy(how=How.XPATH, using ="//link[@href='favicon.ico']")
		public WebElement FaviconLink;
	
// ----------------------------# lOGIN PAGE TEXT ,FIELD AND LINKS #-------------------------------
	
		@FindBy(how=How.XPATH, using ="/html/body/div/div/div/div[2]/div/div/div/div/h2")
		public WebElement LoginHeaderText;
		
		@FindBy(how=How.XPATH, using = "/html/body/div/div/div/div[2]/div/div/div/div/p")
		public WebElement LoginDonthaveanaccountText;
	
		@FindBy(how=How.XPATH, using = "//p[@class='small']/a")
		public WebElement LoginCreateAccountLink;
	
//Login Email Text & Field Display		
		@FindBy(how=How.XPATH, using = "//div[@class='form-group fgroup none']/label[contains(text(),'Email Address')]")
		public WebElement LoginEmailAddressText;
			
		@FindBy(how=How.XPATH, using ="//input[@id ='username']")
		public WebElement LoginEmailAddressField;
		
//Login Invalid data validation
		@FindBy(how=How.XPATH, using ="//span[@id='error-msg']")
		public WebElement LoginEmailPasswordValidation;
			
//Login Password Text & Field Display			
		@FindBy(how=How.XPATH, using = "//div[@class='form-group fgroup pass none']/label[contains(text(),'Password')]")
		public WebElement LoginPasswordText;

		@FindBy(how=How.XPATH, using = "//input[@id ='password']" )
		public WebElement LoginPasswordField;
	
// ------------------------# EYE ICON #-----------------------------------
	
		@FindBy(how=How.XPATH, using ="//i[@class='material-icons']")
		public WebElement LoginEyeIcon;
		
		@FindBy(how=How.XPATH, using ="//i[@id='passwordicon' and contains(text(),'visibility_off')]")
		public WebElement LoginEyeIconVisibilityOff;
		
		@FindBy(how=How.XPATH, using ="//i[@id='passwordicon' and contains(text(),'visibility')]")
		public WebElement LoginEyeIconVisibilityOn;
	
// ------------------------# FORGOT PASSWORD #-----------------------------------
		@FindBy(how=How.XPATH, using ="//input[@id='email']")
		public WebElement ForgotPasswordEmail;
		
		@FindBy(how=How.XPATH, using ="//div[@class = 'col-md-6 col-6 text-right']/a")
		public WebElement ForgotPasswordLink;
		
		@FindBy(how=How.XPATH, using ="//button[@id='submit']")
		public WebElement ForgotPasswordSendResetLinkButton;
		
		
//Reset Password		
		@FindBy(how=How.XPATH, using ="/html/body/div/div/div[2]/div/div/div/div/h2")
		public WebElement ResetPasswordPageHeaderText;
		
		@FindBy(how=How.XPATH, using ="//div[@class='form-group fgroup pass none']/label[text()='New Password']")
		public WebElement ResetPasswordNewPasswordText;
		
		@FindBy(how=How.XPATH, using ="//input[@id='newPassword']")
		public WebElement ResetPasswordNewPasswordField;
		
		@FindBy(how=How.XPATH, using ="//div[@class='form-group fgroup pass none']/label[text()='Confirm Password']")
		public WebElement ResetPasswordConfirmPasswordText;
		
		@FindBy(how=How.XPATH, using ="//input[@id='confirmPassword']")
		public WebElement ResetPasswordConfirmPasswordField;
		
		@FindBy(how=How.XPATH, using ="//i[@class='material-icons']")
		public WebElement ResetPasswordNewandConfirmLoginEyeIcon;
		
		@FindBy(how=How.XPATH,using="//button[@type='submit' and (text()='Change password')]")
		public WebElement ChangePasswordButton;
		
		@FindBy(how=How.XPATH, using = "//p[contains(text(),'I remembered')]")
		public WebElement IrememberedText;
		
		@FindBy(how=How.XPATH, using = "/html/body/div/div/div[2]/div/div/p/a")
		public WebElement MyPasswordTextLink;
		
		@FindBy(how=How.XPATH, using = "//*[@id='error-msg']")
		public WebElement ChangePasswordConfirmationText;
		
		@FindBy(how=How.XPATH, using = "/html/body/div/div/div/div[2]/div/div/div/div/p/a")
		public WebElement ChangePasswordConfirmationPageLoginLink;
		
		@FindBy(how=How.XPATH, using = "//*[@id='newPassword-error']")
		public WebElement ChangePasswordValidationText;
		
		@FindBy(how=How.XPATH, using = "//*[@id='confirmPassword-error']")
		public WebElement ConfirmPasswordValidationText;
		
//	Gmail Xpaths
		@FindBy(how=How.XPATH,using="//*[@id='sign-in-google']/span[2]")
		public WebElement ClickonGmailbutton;

		@FindBy(how=How.XPATH,using="//*[@id='identifierId']")
		public WebElement GmailUserID;
				
		@FindBy(how=How.XPATH,using= "//input[@type='password']")
		public WebElement GmailUserPassword;
		
		@FindBy(how=How.XPATH,using= "//div[@id = 'identifierNext']")
		public WebElement GmailNextButton;

		@FindBy(how=How.XPATH,using= "//*[@id='passwordNext']/span/span")
		public WebElement GmailNextButtontoLogin;	
				
		@FindBy(how=How.XPATH,using= "//*[@id=':2y']/span")
		public List<WebElement> GmailUnreadEmails;
		
		@FindBy(how=How.XPATH,using= "//div[@id=':2a']")
		public WebElement GmailInboxPrimaryText;	
			
		@FindBy(how=How.XPATH,using= "//span[@id=':2y']")
		public WebElement ClickonUnreadPasswordResetMail;
			
		@FindBy(how=How.XPATH,using= "//a[(text()='Reset password')]")
		public WebElement ClickonResetPasswordButton;
		
// ----------------------------# Forgot Password PAGE TEXT ,FIELD AND LINKS #-------------------------------
		@FindBy(how=How.XPATH, using ="/html/body/div/div/div/div[2]/div/div/div/div/h2")
		public WebElement ForgotPasswordPageHeaderText;
		
		@FindBy(how=How.XPATH, using = "/html/body/div/div/div/div[2]/div/div/div/div/p[1]")
		public WebElement IrememberedmypasswordText;
	
		@FindBy(how=How.XPATH, using = "//p[@class='small']/a")
		public WebElement LoginLink;
		
		@FindBy(how=How.XPATH, using ="//*[@id=\"forgotpasswordForm\"]/div[1]/label")
		public WebElement ForgotPasswordEmailAddressText;
		
		@FindBy(how=How.XPATH, using ="//input[@id='email']")
		public WebElement ForgotPasswordEmailAddressField;
		
		@FindBy(how=How.XPATH, using ="//button[@id='submit']")
		public WebElement ForgotPasswordSendRestButton;
		
		@FindBy(how=How.XPATH, using ="//div[@id='register-password-error']")
		public WebElement ForgotPasswordInvalidMailValidation;
		
		@FindBy(how=How.XPATH, using ="//span[@id='error-msg']")
		public WebElement ForgotPasswordMailSentConfirmation;
		
		
	
// ------------------------# REMEMBERME #-----------------------------------
	
		@FindBy(how=How.XPATH, using = "//input[@id='checkboxCustom2']" )
		public WebElement LoginremembermeCheckbox;
	
		@FindBy(how=How.XPATH, using ="//*[@id=\"userLoginForm\"]/div[4]/div/div[1]/div/label")
		public WebElement LoginRemembermetext;
		
// ------------------------# LOGOUT #-----------------------------------	
	
		@FindBy(how=How.XPATH, using ="//li[@class='nav-item dropdown user-d']//a")
		public WebElement LogoutClickonUserProfile;
	
		@FindBy(how=How.XPATH, using = "//ul[@class='dropdown-menu animate slideIn show']/li[2]/a" )
		public WebElement ClickonLogout;
	
//Temporary Path

		@FindBy(how=How.XPATH, using = "//*[@id=\"navbarSupportedContent\"]/ul/li[5]/a")
		public WebElement ClickonAppsTab;


		
		
		

		
}

