package com.applista.qa.testdata;

import org.testng.annotations.DataProvider;
import com.applista.qa.utilities.readexcelConfig;

public class LoginDataProvider {

@DataProvider(name = "UserEmailandPassword")	
public static Object[][] getDatafromDataProvider1(){
		return new Object[][]{{"sunilkumar@mailinator.com","password"}};}

/*------------------------------------------------------------------------
++++++++++++++++++++++ Login Test Data +++++++++++++++++++++++++++++++++++
------------------------------------------------------------------------*/
//-------------# User Login with Valid Credentials #----------------------
@DataProvider(name = "UserValidEmailandPassword")
public static Object[][] getDatafromDataProvider(){
	
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("LoginValidTestData");
	Object[][] data = new Object[rowscount][2];
	for(int i =0;i<rowscount;i++){	
		data[i][0] = readdata.getCellData("LoginValidTestData", i, 0);
		data[i][1] = readdata.getCellData("LoginValidTestData", i, 1);}
	return data;}

//-------------# User Login with Invalid Credentials #----------------------
@DataProvider(name = "UserInvalidEmailandPassword")
public static Object[][] getDatafromDataProvider3(){
		
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("LoginInvalidTestData");
	Object[][] data = new Object[rowscount][2];
	for(int i =0;i<rowscount;i++){	
	
		data[i][0] = readdata.getCellData("LoginInvalidTestData", i, 0);
		data[i][1] = readdata.getCellData("LoginInvalidTestData", i, 1);}
	return data;}
	
//-------------# User Login with Valid & Invalid credentials #---------------
@DataProvider(name = "UserLoginwithValidorInvalidTestData")
public static Object[][] getDatafromDataProvider4(){
		
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("LoginValidInvalidTestData");
	Object[][] data = new Object[rowscount][2];
	for(int i =0;i<rowscount;i++){	
		 
		data[i][0] = readdata.getCellData("LoginValidInvalidTestData", i, 0);
		data[i][1] = readdata.getCellData("LoginValidInvalidTestData", i, 1);}
	return data;}

/*------------------------------------------------------------------------
++++++++++++++++++++++ Forgot Password Test Data +++++++++++++++++++++++++
------------------------------------------------------------------------*/
//User Signup With Valid Credentials	
@DataProvider(name = "forgotPasswordValidEmailTestCase")
public static Object[][] getDatafromDataProvider5(){
  return new Object[][]{{"sunilkumar@mailinator.com"}};}

//User Signup With Invalid Credentials	
@DataProvider(name = "forgotPasswordInvalidEmailTestCase")
public static Object[][] getDatafromDataProvider6(){
  return new Object[][]{{"sunilkumarmailinator.com"}};}

/*------------------------------------------------------------------------
++++++++++++++++++++++ Signup Test Data +++++++++++++++++++++++++++++++++++
------------------------------------------------------------------------*/
//-------------# User Signup With Valid Credentials	#---------------
@DataProvider(name = "UserSignupwithValidTestdata")
public static Object[][] getDatafromDataProvider7(){
			
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("SignupValidTestData");
	Object[][] data = new Object[rowscount][4];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<4; j++){
			data[i][j] = readdata.getCellData("SignupValidTestData", i, j);}}
	return data;}
	
//-------------# User Signup With Invalid Credentials #---------------
@DataProvider(name = "UserSignupwithInvalidTestdata")
public static Object[][] getDatafromDataProvider8(){
			
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("SignupInvalidTestData");
	Object[][] data = new Object[rowscount][4];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<4; j++){
			data[i][j] = readdata.getCellData("SignupInvalidTestData", i, j);}}
	return data;}

//-------------# Signup Single Field Valid & Invalid TestData #---------------
@DataProvider(name = "SignupValid&InvalidSingleFieldTestData")
public static Object[][] getDatafromDataProvider9(){
				
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("Signup_SingleFieldValidTestData");
	Object[][] data = new Object[rowscount][4];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<4; j++){
			data[i][j] = readdata.getCellData("Signup_SingleFieldValidTestData", i, j);}}
	return data;}

//-------------# Signup Two Fields Valid & Invalid TestData #---------------
@DataProvider(name = "SignupValid&InvalidTwoFieldsTestData")
public static Object[][] getDatafromDataProvider10(){
					
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("Signup_TwoFieldsValidTestData");
	Object[][] data = new Object[rowscount][4];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<4; j++){
			data[i][j] = readdata.getCellData("Signup_TwoFieldsValidTestData", i, j);}}
	return data;}

//-------------# Signup Three Fields Valid & Invalid TestData #---------------
@DataProvider(name = "SignupValid&InvalidSThreeFieldsTestData")
public static Object[][] getDatafromDataProvider11(){
						
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("Signup_ThreeFieldsValidTestData");
	Object[][] data = new Object[rowscount][4];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<4; j++){
			data[i][j] = readdata.getCellData("Signup_ThreeFieldsValidTestData", i, j);}}
	return data;}

/*------------------------------------------------------------------------
++++++++++++++++++++++ Organization Test Data ++++++++++++++++++++++++++++
------------------------------------------------------------------------*/
//-------------# User Organization With Valid Test Data #---------------
@DataProvider(name = "OrganizationValidTestData")
public static Object[][] getDatafromDataProvider12(){
			
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("OrganizationValidTestData");
	Object[][] data = new Object[rowscount][6];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<6; j++){
			data[i][j] = readdata.getCellData("OrganizationValidTestData", i, j);}}
	return data;}

//-------------# User Organization With Invalid Test Data #----------------
@DataProvider(name = "OrganizationInvalidTestData")
public static Object[][] getDatafromDataProvider13(){
			
	readexcelConfig readdata = new readexcelConfig();
	int rowscount = readdata.getRowCount("OrganizationInvalidTestData");
	Object[][] data = new Object[rowscount][6];
	for(int i =0;i<rowscount;i++){	
		for(int j=0; j<6; j++){
			data[i][j] = readdata.getCellData("OrganizationInvalidTestData", i, j);}}
	return data;}

//-------------# Forgot Password With Valid Test Data #----------------
@DataProvider(name = "ResetPasswordValidTestData")
public static Object[][] getDatafromDataProvider14(){
  return new Object[][]{{"password","password"}};}

//-------------# Forgot Password With Invalid Test Data #----------------
@DataProvider(name = "ResetPasswordInvalidTestData")
public static Object[][] getDatafromDataProvider15(){
return new Object[][]{{"pass","passw"}};}
}