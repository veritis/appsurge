package com.applista.qa.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.applista.qa.pages.LoginPage;

public class Action {
	
public static void verifyPagetitle(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,100);
	    wait.until(ExpectedConditions.titleContains("applista portal"));}

public static boolean mouseHover(WebDriver driver) {
	LoginPage login = new LoginPage(driver);
	Actions action = new Actions(driver);
	action.moveToElement(login.LoginButton).build().perform();
	return true;}

public static void mouseHoverandClick(WebDriver driver) {
	LoginPage login = new LoginPage(driver);
	Actions action = new Actions(driver);
	action.moveToElement(login.LoginCreateAccountLink).click(login.LoginCreateAccountLink).build().perform();}

//Email validation using regex in java
private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
private static final String FIRSTNAME_LASTNAME = "";

// static Pattern object, since pattern is fixed
private static Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
private static Pattern firstnameAndLastnamePattern = Pattern.compile(FIRSTNAME_LASTNAME);

// non-static Matcher object because it's created from the input String
private static Matcher matcher;
public static boolean validateEmail(String email) {
	matcher = pattern.matcher(email);
	return matcher.matches();}

public static boolean validateFirstnameAndLastname(String name) {
	matcher = firstnameAndLastnamePattern.matcher(name);
	return matcher.matches();}

//wait statements

public static void waitElementByWebElement(WebDriver driver ,WebElement element,int numberOfSeconds) {	
	WebDriverWait wait = new WebDriverWait(driver, numberOfSeconds);
	wait.until( ExpectedConditions.visibilityOf(element));}

}

