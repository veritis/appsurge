package com.applista.qa.actions;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import com.applista.qa.pages.LoginPage;
import com.applista.qa.pages.SignupPage;
import com.applista.qa.utilities.Log;
import com.applista.qa.utilities.ReadConfig;

public class LoginAction {
	
	public static ReadConfig readconfig =new ReadConfig();
	
//*******************************************************************************************
//+++++++++++++++++++++++++++++++++ # LOGIN TEST CASES # ++++++++++++++++++++++++++++++++++++
//*******************************************************************************************	
/*-------------------------------------------------------------------------
	AL-129	Applista >Login> Check page title with valid URL
	AL-130	Applista >Login> Check page title with Invalid URL
----------------------------------------------------------------------------*/
public static void loginPageTitleTestCase(WebDriver driver ){	
	Log.startTestCase("Login Home Page Title Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Login"));}
	
/*-------------------------------------------------------------------------
	//AL-91 Applista > Check Website Fevicon
---------------------------------------------------------------------------*/
public static void loginFeviconTestCase(WebDriver driver) throws Exception {
	Log.startTestCase("Fevicon Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	LoginPage login = new LoginPage(driver);
		
	Assert.assertTrue(login.LoginvalidEmailAddress.isDisplayed());
	login.LoginvalidEmailAddress.clear();
	login.LoginvalidEmailAddress.sendKeys("sunilkumar@mailinator.com");
	Action.waitElementByWebElement(driver, login.LoginvalidEmailAddress, 20);
	Log.info("Entered Valid User Email Address");

	Assert.assertTrue(login.LoginvalidPassword.isDisplayed());
	login.LoginvalidPassword.clear();
	login.LoginvalidPassword.sendKeys("password");
	Action.waitElementByWebElement(driver, login.LoginvalidPassword, 20);
	Log.info("Entered Valid User Password");
	
	Assert.assertTrue(login.LoginButton.isDisplayed());
	Assert.assertTrue(login.LoginButton.isEnabled());
	login.LoginButton.click();
	Log.info("Click Action Performed on Login Button.");
	
	Assert.assertEquals("OVERVIEW", "OVERVIEW");
	Log.info("Verfied Next Page Header Text");
		
	LoginPage favicon = new LoginPage(driver);
	Assert.assertEquals(favicon.FaviconLink.getTagName(), "link");
	Log.info("Verfied Application Favicon");}

/*-------------------------------------------------------------------------
	AL-131 Applista > Login > Check Login page
---------------------------------------------------------------------------*/
public static void loginHomePageTestCase(WebDriver driver) throws Exception {
	Log.startTestCase("login Home Page Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	LoginPage login = new LoginPage(driver);
	
	Assert.assertTrue(login.LoginHeaderText.isDisplayed());
	Log.info("Login Header Text Verfied");
	
	Assert.assertTrue(login.LoginDonthaveanaccountText.isDisplayed());
	Log.info("Login Dont have an account Text Verfied");
	
	Assert.assertTrue(login.LoginCreateAccountLink.isDisplayed());
	Log.info("Login Create Account Link Verfied");
	
	Assert.assertTrue(Action.mouseHover(driver));
	
	Assert.assertTrue(login.LoginEmailAddressText.isDisplayed());
	Log.info("Login Email Address Text Verfied");
	
	Assert.assertTrue(login.LoginEmailAddressField.isDisplayed());
	Log.info("Login Email Address Field Text Verfied");
	
	Assert.assertTrue(login.LoginPasswordText.isDisplayed());
	Log.info("Login Password Text Verfied");
	
	Assert.assertTrue(login.LoginPasswordField.isDisplayed());	
	Log.info("Login Password Field Verfied");
	
    Assert.assertTrue(login.LoginButton.isDisplayed()); 
    Log.info("Login Login Button Verfied");
    
	Assert.assertTrue(login.LoginEyeIcon.isDisplayed());
	Log.info("Login Login Eye Icon Verfied");	
	
	Assert.assertTrue(login.ForgotPasswordLink.isDisplayed());
	Log.info("Login Forgot Password Link Verfied");
	
	Assert.assertTrue(Action.mouseHover(driver));
	Thread.sleep(3000);}

/*--------------------------------------------------------------------------------
	AL-132 Applista > Login > Check Don't have an Account ? Create account link
---------------------------------------------------------------------------------*/
public static void loginCreateAccountLinkTestCase(WebDriver driver) throws Exception {
//	Log.startTestCase("Create Account Link Test Case");
	LoginPage login = new LoginPage(driver);
	
	Assert.assertTrue(login.LoginDonthaveanaccountText.isDisplayed());
	Assert.assertTrue(login.LoginCreateAccountLink.isDisplayed());
	login.LoginCreateAccountLink.isEnabled();
//	Log.info("Login Create Account Link Verfied");
	Action.mouseHoverandClick(driver);
//	Log.info("Click Action Performed on Create Account Link.");
//	Assert.assertEquals("SIGN UP", "SIGN UP");
//	Log.info("Verfied Next Page Header Text");
	Thread.sleep(1000);}

/*---------------------------------------------------------------------------------------
	AL-133 Applista > Login > Check Login with valid Email and Valid Password credentials
	AL-141 Applista > Login > Check "LOGIN" button with valid validations
	AL-157 Applista > Logout > Check "Logout" for the account
-----------------------------------------------------------------------------------------*/
public static void loginValidTestCase(WebDriver driver ,String Email,String Password ) throws Exception {
	Log.startTestCase("login Valid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	LoginPage login = new LoginPage(driver);
	
	Assert.assertTrue(login.LoginvalidEmailAddress.isDisplayed());
	login.LoginvalidEmailAddress.clear();
	login.LoginvalidEmailAddress.sendKeys(Email);
	Action.waitElementByWebElement(driver, login.LoginvalidEmailAddress, 20);
	Log.info("Entered Valid User Email Address");
	
	Assert.assertTrue(login.LoginvalidPassword.isDisplayed());
	login.LoginvalidPassword.clear();
	login.LoginvalidPassword.sendKeys(Password);
	Action.waitElementByWebElement(driver, login.LoginvalidPassword, 20);
	Log.info("Entered Valid User Password");

	Assert.assertTrue(login.LoginButton.isDisplayed());
	Assert.assertTrue(login.LoginButton.isEnabled());
	login.LoginButton.click();
	Log.info("Click Action Performed on Login Button.");
	
	Assert.assertEquals("OVERVIEW", "OVERVIEW");
    Log.info("Verfied Next Page Header Text");}

/*-------------------------------------------------------------------------------------------
	AL-134 Applista > Login > Check Login with Invalid Email and Invalid Password credentials
	AL-142 Applista > Login > Check "LOGIN" button with Invalid validations
---------------------------------------------------------------------------------------------*/
public static void loginInvalidTestCase(WebDriver driver ,String Email,String Password ) throws Exception {
	Log.startTestCase("login Invalid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	LoginPage login = new LoginPage(driver);
	
	Assert.assertTrue(login.LoginInvalidEmailAddress.isDisplayed());
	login.LoginInvalidEmailAddress.clear();
	login.LoginInvalidEmailAddress.sendKeys(Email);
	Action.waitElementByWebElement(driver, login.LoginInvalidEmailAddress, 20);
	Log.info("Entered Invalid User Email Address");

	Assert.assertTrue(login.LoginInvalidPassword.isDisplayed());
	login.LoginInvalidPassword.clear();
	login.LoginInvalidPassword.sendKeys(Password);
	Action.waitElementByWebElement(driver, login.LoginInvalidPassword, 20);
	Log.info("Entered Invalid User Password");

	Assert.assertTrue(login.LoginButton.isDisplayed());
	Assert.assertTrue(login.LoginButton.isEnabled());
	login.LoginButton.click();
	Log.info("Click Action Performed on Login Button.");
	
	Assert.assertTrue(login.LoginInvalidEmailPasswordValidationMessage.isDisplayed());
	Action.waitElementByWebElement(driver, login.LoginInvalidEmailPasswordValidationMessage, 20);
	Log.info("Verfied Invalid EmailPassword Validation Message Text");}

/*-------------------------------------------------------------------------------------
	AL-135 Applista > Login > Check Login with Valid Email and Invalid Password credentials
	AL-136 Applista > Login > Check Login with Invalid Email and Valid Password credentials
-------------------------------------------------------------------------------------*/
public static void userLoginwithValidInvalidDataTestCase(WebDriver driver ,String Email,String Password ) throws Exception {
	Log.startTestCase("Login with Valid & Invalid Data Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage login = new LoginPage(driver);
	
	Assert.assertTrue(login.LoginInvalidEmailAddress.isDisplayed());
	login.LoginInvalidEmailAddress.clear();
	login.LoginInvalidEmailAddress.sendKeys(Email);
	Action.waitElementByWebElement(driver, login.LoginInvalidEmailAddress, 20);
	Log.info("Entered Valid or Invalid User Email Address");

	Assert.assertTrue(login.LoginInvalidPassword.isDisplayed());
	login.LoginInvalidPassword.clear();
	login.LoginInvalidPassword.sendKeys(Password);
	Action.waitElementByWebElement(driver, login.LoginInvalidPassword, 20);
	Log.info("Entered Valid or Invalid User Password");
		
	Assert.assertTrue(login.LoginButton.isDisplayed());
	Assert.assertTrue(login.LoginButton.isEnabled());
	login.LoginButton.click();
	Log.info("Click Action Performed on Login Button.");
	
	boolean EmailAddressIsValid =false,PasswordIsValid=false;
	
	EmailAddressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
	PasswordIsValid = (Password.length()>=8 && Password.length()<=16 ) ? true : false;
		
 if(!EmailAddressIsValid){
	Assert.assertTrue(login.LoginEmailPasswordValidation.isDisplayed());
 	Action.waitElementByWebElement(driver, login.LoginEmailPasswordValidation, 20);
	Log.info("Verfied Login Valid or Invalid Email Validation");}

 if(!PasswordIsValid){
	Assert.assertTrue(login.LoginEmailPasswordValidation.isDisplayed());
 	Action.waitElementByWebElement(driver, login.LoginEmailPasswordValidation, 20);
	Log.info("Verfied Login Valid or Invalid Password Validation");}}

/*-------------------------------------------------------------------------------------
AL-116 Applista > Signup/Login > Password/Confirm password > Check Eye Icon
--------------------------------------------------------------------------------*/
public static void loginPasswordEyeIconTestCase(WebDriver driver) throws Exception {	
	Log.startTestCase("Login Password Eye Icon Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage login = new LoginPage(driver);

	login.LoginvalidEmailAddress.clear();
	login.LoginvalidEmailAddress.sendKeys("sunilkumar@mailinator.com");
	Action.waitElementByWebElement(driver, login.LoginvalidEmailAddress, 20);
	Log.info("Entered Valid User Email");
	
	login.LoginvalidPassword.clear();
	login.LoginvalidPassword.sendKeys("password");
	Action.waitElementByWebElement(driver, login.LoginvalidPassword, 20);
	Log.info("Entered Valid User Password");

	//Eye Icon Visibility by default is in disable mode 
	Assert.assertTrue(login.LoginEyeIconVisibilityOff.isDisplayed());
	
	login.LoginEyeIconVisibilityOff.click();
	Log.info("Click Action Performed on Visibility Off Password Eye Icon.");

	//Click on Eye Icon Visibility or text entered in password field will be visual 
	Assert.assertTrue(login.LoginEyeIconVisibilityOn.isDisplayed());
	login.LoginPasswordField.isDisplayed();
	Log.info("Entered password text is Visible");
	
	login.LoginEyeIconVisibilityOn.click();
	Log.info("Click Action Performed on Visibility On Password Eye Icon.");
	Log.info("Entered password text is not Visible");
}
//-------------------------------------------------------------------------------------
//AL-137 Applista > Login > Check "Remember me" Check box
//AL-139 Applista > Login > Remember me > Check "Remember me" with selecting check box(Checked)
//AL-140 Applista > Login > Remember me > Check "Remember me" without selecting check box(Unchecked)
//-------------------------------------------------------------------------------------

/*-------------------------------------------------------------------------------------
	AL-157 Applista >Logout > Check "Logout" for the account
--------------------------------------------------------------------------------------*/
public static void logoutTestCase(WebDriver driver) throws Exception {
	
	SignupPage logout = new SignupPage(driver);
	
	Action.waitElementByWebElement(driver, logout.LogoutClickonUserProfile, 20);	
	Assert.assertTrue(logout.LogoutClickonUserProfile.isDisplayed());
	Log.info("User Profile Text Verfied");
	
	logout.LogoutClickonUserProfile.isEnabled();
	logout.LogoutClickonUserProfile.click();
	Log.info("Click Action Performed on User Profile Button.");	
	
	Action.waitElementByWebElement(driver, logout.ClickonLogout, 20);	
	Assert.assertTrue(logout.ClickonLogout.isDisplayed());
	Log.info("Logout Text Verfied");
	
	logout.ClickonLogout.isEnabled();
	logout.ClickonLogout.click();
	Log.info("Click Action Performed on Logout Button.");	
	Log.info("Logout Verfied and Done Succesfully");}

/*-------------------------------------------------------------------------------------
	AL-145 Applista > Forgot Password > Check "Forget Password" link
---------------------------------------------------------------------------------------*/  
public static void forgotPasswordLinkTestCase(WebDriver driver) throws Exception {
//	Log.startTestCase("Forgot Password Link Test Case");
	LoginPage forgotPassword = new LoginPage(driver);
	 
	Assert.assertTrue(forgotPassword.ForgotPasswordLink.isDisplayed());
//	Log.info("Forgot Password Link Verfied");
	Assert.assertTrue(Action.mouseHover(driver));
	forgotPassword.ForgotPasswordLink.click();
//	Log.info("Click Action Performed on Forgot Password Link.");	
	Assert.assertEquals("FORGOT PASSWORD?", "FORGOT PASSWORD?");}
//	Log.info("Verfied Next Page Header Text");}

/*-----------------------------------------------------------------------
	AL-145 Applista > Forgot Password > Check "Forget Password" link
	AL-148 Applista > Forgot Password > Check Reset Password Page
	AL-151 Applista > Forgot Password > Check "I remembered my password.Login" link
------------------------------------------------------------------------*/
public static void forgotPasswordPageTestCase(WebDriver driver) throws Exception {
	Log.startTestCase("Forgot Password Page Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage forgotPassword = new LoginPage(driver);
	
	Assert.assertTrue(forgotPassword.ForgotPasswordPageHeaderText.isDisplayed());
	Log.info("Forgot Password Page Header Text Verfied");
	
	Assert.assertTrue(forgotPassword.IrememberedmypasswordText.isDisplayed());
	Log.info("I remembered my password Text Verfied");
	
	Assert.assertTrue(forgotPassword.LoginLink.isDisplayed());
	Log.info("Login Link Verfied");
	
	Assert.assertTrue(Action.mouseHover(driver));
	
	Assert.assertTrue(forgotPassword.ForgotPasswordEmailAddressText.isDisplayed());
	Log.info("Forgot Password Email Address Text Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordEmailAddressField.isDisplayed());
	Log.info("Forgot Password Email Address Field Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordSendRestButton.isDisplayed());
	Log.info("Forgot Password Send Rest Button Verfied");
	
	Assert.assertTrue(Action.mouseHover(driver));
	
	forgotPassword.LoginLink.click();
	Log.info("Click Action performed on I remembered my password Text Login Link");}

/*----------------------------------------------------------------------------------------------------
	AL-146 Applista > Forgot Password > Check Title of the page for "Forget Password?" with valid URL
	AL-147 Applista > Forgot Password > Check Title of the page for "Forget Password?" with Invalid URL
-------------------------------------------------------------------------------------------------------*/
public static void forgotPasswordPageTitleTestCase(WebDriver driver ){	
	Log.startTestCase("Forgot Password Page Title Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Forgot password"));}

/*------------------------------------------------------------------------------------------------
	AL-149 Applista > Forgot Password > Check "Email Address " with valid conditions
	AL-152 Applista > Forgot Password > Check "Reset Password" button with valid validations
--------------------------------------------------------------------------------------------------*/
public static void forgotPasswordValidEmailTestCase(WebDriver driver,String Email ) throws Exception {
	Log.startTestCase("Forgot Password Valid Email Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage login = new LoginPage(driver);
	
	login.ForgotPasswordEmail.clear();
	login.ForgotPasswordEmail.sendKeys("apllistaweb@gmail.com");
	Action.waitElementByWebElement(driver, login.ForgotPasswordEmail, 20);	
	Log.info("Entered Valid User Email Address");

	Assert.assertTrue(login.ForgotPasswordSendResetLinkButton.isEnabled());
	login.ForgotPasswordSendResetLinkButton.click();
	Action.waitElementByWebElement(driver, login.ForgotPasswordEmail, 20);	
	Log.info("Click Action Performed on Forgot Password Send Reset Link Button.");
	
	boolean EmailAddressIsValid =false;
	
	EmailAddressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
		
 if(!EmailAddressIsValid){
	Assert.assertTrue(login.ForgotPasswordMailSentConfirmation.isDisplayed());
	Action.waitElementByWebElement(driver, login.ForgotPasswordMailSentConfirmation, 20);	
	Log.info("Verfied Forgot Password Mail Sent Confirmation Message Text");}}

/*------------------------------------------------------------------------------------------------
	AL-150 Applista > Forgot Password > Check "Email Address " with Invalid conditions
	AL-153 Applista > Forgot Password > Check "Reset Password" button with Invalid validations
--------------------------------------------------------------------------------------------------*/
public static void forgotPasswordInvalidEmailTestCase(WebDriver driver,String Email ) throws Exception {
	Log.startTestCase("Forgot Password Invalid Email Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage login = new LoginPage(driver);
	
	login.ForgotPasswordEmail.clear();
	login.ForgotPasswordEmail.sendKeys(Email);
	Action.waitElementByWebElement(driver, login.ForgotPasswordEmail, 20);
	Log.info("Entered Invalid User Email Address");

	Assert.assertTrue(login.ForgotPasswordSendResetLinkButton.isEnabled());
	login.ForgotPasswordSendResetLinkButton.click();
	Action.waitElementByWebElement(driver, login.ForgotPasswordSendResetLinkButton, 20);
	Log.info("Click Action Performed on Forgot Password Send Reset Link Button.");

	boolean EmailAddressIsValid =false;
	EmailAddressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
		
 if(!EmailAddressIsValid){
	Assert.assertTrue(login.ForgotPasswordInvalidMailValidation.isDisplayed());
	Action.waitElementByWebElement(driver, login.ForgotPasswordInvalidMailValidation, 20);
	Log.info("Verfied Forgot Password Invalid Mail Validation");}}

/*-------------------------------------------------------------------------------------
	AL-151 Applista > Forgot Password > Check "I remembered my password.Login" link
--------------------------------------------------------------------------------------*/
public static void forgotPasswordIrememberedmypasswordLoginTestCase(WebDriver driver) throws Exception {
	LoginPage forgotPasswordPage = new LoginPage(driver);
	
	Assert.assertTrue(forgotPasswordPage.IrememberedmypasswordText.isDisplayed());
	Log.info("I remembered my password Text Verfied");
	
	Assert.assertTrue(forgotPasswordPage.LoginLink.isDisplayed());
	forgotPasswordPage.LoginLink.click();
	Log.info("Click Action performed on I remembered my password Text Login Link ");}

/*-------------------------------------------------------------------------------------
	AL-498 Applista > Forgot Password > Check Reset Password mail body
--------------------------------------------------------------------------------------*/
public static void forgotPasswordResetPasswordMailBodyTestCase(WebDriver driver) throws Exception {
	Log.startTestCase("Forgot Password Valid Email Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	
	LoginPage login = new LoginPage(driver);
	LoginPage gmail = new LoginPage(driver);
	
	login.ForgotPasswordEmail.clear();
	login.ForgotPasswordEmail.sendKeys("apllistaweb@gmail.com");
	Action.waitElementByWebElement(driver, login.ForgotPasswordEmail, 20);	
	Log.info("Entered Valid User Email Address");

	Assert.assertTrue(login.ForgotPasswordSendResetLinkButton.isEnabled());
	login.ForgotPasswordSendResetLinkButton.click();
	Action.waitElementByWebElement(driver, login.ForgotPasswordEmail, 20);	
	Log.info("Click Action Performed on Forgot Password Send Reset Link Button.");
	
//	To open new tab on existing Opened browser
	((JavascriptExecutor)driver).executeScript("window.open()");
	
//	To get Existing window count
	ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
	
//	String window = tab.get(1);
    driver.switchTo().window(tab.get(1));
    Thread.sleep(3000);
    
//  Navigated to Mail box
    driver.get("https://accounts.google.com");
    Thread.sleep(2000);
      
    gmail.GmailUserID.sendKeys("apllistaweb@gmail.com");
	Log.info("Entered valid Gmail ID" );
	Thread.sleep(1000);
	
	gmail.GmailNextButton.click();	
	Log.info("Click action performed on GmailNextButton.");
	Thread.sleep(1000);
	
	gmail.GmailUserPassword.sendKeys("Testing@123");
	Log.info("Entered valid Gmail Password" );
	Thread.sleep(1000);
	
	gmail.GmailNextButtontoLogin.click();
	Log.info("Click action performed on GmailNextButton.");
	Thread.sleep(3000);
	
	driver.get("https://mail.google.com/mail/u/0/#inbox"); 
	
	Assert.assertTrue(gmail.GmailInboxPrimaryText.isDisplayed());
	Log.info("Verfied Next Page Primary header Text");
	
	List<WebElement> Unreademail = gmail.GmailUnreadEmails;
	String mailname = "applistacore password reset";
	String resetButton = "Reset Password";
	
//	getting count of all unread web elements
	for(int i=0;i<Unreademail.size();i++){
		
//		checking email is displayed
		if(Unreademail.get(i).isDisplayed()==true){
			if(Unreademail.get(i).getText().equals(mailname)){
				Log.info("Applista core reset password Mail Recevied");
				
				gmail.ClickonUnreadPasswordResetMail.click();
				Thread.sleep(1000);
				Log.info("Click action performed on applistacore password reset unread mail ");
				
				Assert.assertEquals("noreply@applista.com", "noreply@applista.com");
				Log.info("Verfied applista core password Mail ");
				Thread.sleep(1000);
				
				gmail.ClickonResetPasswordButton.click();
				Log.info("Click action performed on Reset Password Button ");
				
			}else{	
				Log.info("Applista core reset password Mail not Recevied");}			
		}
	}
		Assert.assertEquals("RESET PASSWORD", "RESET PASSWORD");
		Log.info("Verfied Next Page Header Text");
		Thread.sleep(3000);}   
	
/*-------------------------------------------------------------------------------------
	AL-499 Applista > Forgot Password > Reset Password Mail > Check Reset password page
--------------------------------------------------------------------------------------*/
public static void resetPasswordPageTestCase(WebDriver driver) throws Exception {
	
	Log.startTestCase("Reset Password Page Test Case");
	LoginPage resetPassword = new LoginPage(driver);
	
	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window((String) tabs.get(2));	
	
	Assert.assertTrue(resetPassword.ResetPasswordPageHeaderText.isDisplayed());
	Log.info("Reset Password Page Header Text Verfied");
	
	Assert.assertTrue(resetPassword.ResetPasswordNewPasswordText.isDisplayed());
	Log.info("Reset Password NewPassword Text Verfied");
	
	Assert.assertTrue(resetPassword.ResetPasswordNewPasswordField.isDisplayed());
	Log.info("Reset Password NewPassword Field Verfied");
	
	Assert.assertTrue(resetPassword.ResetPasswordConfirmPasswordText.isDisplayed());
	Log.info("Reset Password Confirm Password Text Verfied");
		
	Assert.assertTrue(resetPassword.ResetPasswordConfirmPasswordField.isDisplayed());
	Log.info("Reset Password Confirm Password Field Verfied");
	
	Assert.assertTrue(resetPassword.ResetPasswordNewandConfirmLoginEyeIcon.isDisplayed());
	Log.info("Reset Password New and Confirm Login EyeIcon Verfied");
	
	Assert.assertTrue(resetPassword.IrememberedText.isDisplayed());
	Log.info("Reset Password I remembered Text Verfied");
	
	Assert.assertTrue(resetPassword.MyPasswordTextLink.isDisplayed());
	Log.info("Reset Password My Password Text Link Verfied");
		
	resetPassword.ChangePasswordButton.isDisplayed();
	Log.info("Change Password Button Diplayed and in Enable Mode");}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	AL-500 Applista > Forgot Password > Reset Password Mail >Reset password page > Check New Password and Confirm Password fields with valid validations
	AL-504 Applista > Forgot Password > Reset Password Mail >Reset password page > Check "Change Password" button with valid & Invalid validations
-------------------------------------------------------------------------------------------------------------------- */
public static void changePasswordValidTestCase(WebDriver driver,String NewPassword,String ConfirmPassword) throws Exception {	
	Log.startTestCase("Reset Password Valid Test Case");
	LoginPage resetPassword = new LoginPage(driver);

	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window((String) tabs.get(2));	
	
	resetPassword.ResetPasswordNewPasswordField.sendKeys(NewPassword);
	Log.info("Reset Password - Entered Valid New Password");
	
	resetPassword.ResetPasswordConfirmPasswordField.sendKeys(ConfirmPassword);
	Log.info("Reset Password - Entered Valid Confirm Password");

	resetPassword.ChangePasswordButton.click();
	Log.info("Click action performed on Change Password Button ");
	Thread.sleep(1000);
	
	Assert.assertEquals("RESET PASSWORD", "RESET PASSWORD");
	Log.info("Verfied Next Page Header Text");
	Thread.sleep(3000);
	
	boolean ChangePasswordIsValid =false,ConfirmPasswordIsValid= false;
	
	ChangePasswordIsValid = (NewPassword.length()>=8 && NewPassword.length()<=16) ? true : false;	
	ConfirmPasswordIsValid = (ConfirmPassword.length()>=8 && ConfirmPassword.length()<=16 ) ? true : false;
		
 if(!ChangePasswordIsValid){
	Assert.assertTrue(resetPassword.ChangePasswordConfirmationText.isDisplayed());
	Log.info("Verfied Change Password display Message Text");}
 
 if(!ConfirmPasswordIsValid){
		Assert.assertTrue(resetPassword.ChangePasswordConfirmationText.isDisplayed());
		Log.info("Verfied Change Password display Message Text");}
		
		resetPassword.ChangePasswordConfirmationPageLoginLink.click();
		Log.info("Click action performed on Login  link to navigate to Login Page");}
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	AL-501 Applista > Forgot Password > Reset Password Mail >Reset password page > Check New Password and Confirm Password fields with Invalid validations
	AL-504 Applista > Forgot Password > Reset Password Mail >Reset password page > Check "Change Password" button with valid & Invalid validations
	AL-502 Applista > Forgot Password > Reset Password Mail >Reset password page > Check New Password with valid Validations and Confirm Password with Invalid validations
	AL-503 Applista > Forgot Password > Reset Password Mail >Reset password page > Check New Password with Invalid Validations and Confirm Password with valid validations
------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public static void changePasswordInvalidTestCase(WebDriver driver,String NewPassword,String ConfirmPassword) throws Exception {	
	Log.startTestCase("Reset Password Invalid Test Case");
	LoginPage resetPassword = new LoginPage(driver);

	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window((String) tabs.get(2));	
	
	resetPassword.ResetPasswordNewPasswordField.sendKeys(NewPassword);
	Log.info("Reset Password - Entered Invalid New Password");
	
	resetPassword.ResetPasswordConfirmPasswordField.sendKeys(ConfirmPassword);
	Log.info("Reset Password - Entered Invalid Confirm Password");

	resetPassword.ChangePasswordButton.click();
	Log.info("Click action performed on Change Password Button ");
	Thread.sleep(1000);
	
	Assert.assertEquals("RESET PASSWORD", "RESET PASSWORD");
	Log.info("Verfied Next Page Header Text");
	Thread.sleep(3000);
	
	boolean ChangePasswordIsInvalid =false,ConfirmPasswordIsInvalid= false;
	
	ChangePasswordIsInvalid = (NewPassword.length()>=8 && NewPassword.length()<=16) ? true : false;	
	ConfirmPasswordIsInvalid = (ConfirmPassword.length()>=8 && ConfirmPassword.length()<=16 ) ? true : false;
		
 if(!ChangePasswordIsInvalid){
	Assert.assertTrue(resetPassword.ChangePasswordValidationText.isDisplayed());
	Log.info("Verfied Change Password Validation Text");}
 
 if(!ConfirmPasswordIsInvalid){
	Assert.assertTrue(resetPassword.ConfirmPasswordValidationText.isDisplayed());
	Log.info("Verfied Confirm Password Validation Text");}
		
	resetPassword.MyPasswordTextLink.click();
	Log.info("Click action performed on Login  link to navigate to Login Page");}
/*--------------------------------------------------------------------------------------------------------------------
AL-505	Applista > Forgot Password > Reset Password Mail >Reset password page > Check I remembered my password Link
-------------------------------------------------------------------------------------------------------------------- */

}


