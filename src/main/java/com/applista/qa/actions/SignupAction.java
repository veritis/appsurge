package com.applista.qa.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.applista.qa.pages.SignupPage;
import com.applista.qa.utilities.Log;

public class SignupAction {

/*-******************************************************************************************
+++++++++++++++++++++++++++++++++ # SIGNUP TEST CASES # ++++++++++++++++++++++++++++++++++++
//*******************************************************************************************-*/		
/*-------------------------------------------------------------------------------
	AL-93	Applista > Signup > Check Already have a Account ? Login link
	AL-132  Applista > Login > Check Don't have an Account ? Create account link
---------------------------------------------------------------------------------*/		
public static void signupLoginlink(WebDriver driver) throws Exception {	
	SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(signup.SignupAlreadyhaveanaccountText.isDisplayed());
	Assert.assertTrue(signup.SignupLoginLink.isDisplayed());
	signup.SignupLoginLink.isEnabled();
	Action.mouseHoverandClick(driver);
	Assert.assertEquals("LOG IN", "LOG IN");
	Thread.sleep(1000);
	Log.info("Sign up_Already have a Account ? Login link & Login_Don't have an Account ? Create account link verified and clicked succesfully");}

/*-------------------------------------------------------------------------
	AL-89	Applista > Signup> Check page title with valid URL 	
	AL-90	Applista > Signup> Check page title with Invalid URL
-----------------------------------------------------------------------------*/	
public static void signupPageTitleTestCase(WebDriver driver ) throws Exception{	
	Log.startTestCase("Signup Page Title Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Sign up"));}

/*-------------------------------------------------------------------------
  	AL-92	Applista > Signup > Check Signup page
-----------------------------------------------------------------------------*/
public static void signupPageTestCase(WebDriver driver) throws Exception {	
	Log.startTestCase("Signup Page Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(signup.SignupHeaderText.isDisplayed());
	Log.info("Signup Header Text Verfied");
	Assert.assertTrue(Action.mouseHover(driver));
	Assert.assertTrue(signup.SignupAlreadyhaveanaccountText.isDisplayed());
	Log.info("Signup Already have an accountText Verfied");
	Assert.assertTrue(signup.SignupLoginLink.isDisplayed());
	Log.info("Signup Login Link Verfied");
	Assert.assertTrue(signup.SignupFirstNameText.isDisplayed());
	Log.info("Signup First Name Text Verfied");
	Assert.assertTrue(signup.SignupFirstNameField.isDisplayed());
	Log.info("Signup First Name Field Verfied");
	Assert.assertTrue(signup.SignupLastNameText.isDisplayed());
	Log.info("Signup Last Name Text Verfied");
	Assert.assertTrue(signup.SignupLastNameField.isDisplayed());
	Log.info("Signup Last Name Field Verfied");
	Assert.assertTrue(signup.SignupEmailAddressText.isDisplayed());
	Log.info("Signup Email Address Text Verfied");
	Assert.assertTrue(signup.SignupEmailAddressField.isDisplayed());
	Log.info("Signup Email Address Field Verfied");
	Assert.assertTrue(signup.SignupPasswordText.isDisplayed());
	Log.info("Signup Password Text Verfied");
	Assert.assertTrue(signup.SignupPasswordField.isDisplayed());
	Log.info("Signup Password Field Verfied");
	Assert.assertTrue(signup.SignupEyeIcon.isDisplayed());
	Log.info("Signup Eye Icon Verfied");
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());
	Log.info("Signup Continue Button Verfied");
	Thread.sleep(5000);}

/*------------------------------------------------------------------------------
	AL-94  Applista > Signup > Check Signup with new user with valid conditions
	AL-117 Applista > Signup > Check "CONTINUE" button with valid validations
---------------------------------------------------------------------------------*/
public static void signupValidTestCase(WebDriver driver ,String FirstName,String LastName,String Email,String Password ) throws Exception {	
	Log.startTestCase("signup Valid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(signup.SignupvalidFirstName.isDisplayed());
	signup.SignupvalidFirstName.clear();
	signup.SignupvalidFirstName.sendKeys(FirstName);
	Action.waitElementByWebElement(driver, signup.SignupvalidFirstName, 20);
	Log.info("Entered Valid New User First Name");

	
	Assert.assertTrue(signup.SignupvalidLastName.isDisplayed());
	signup.SignupvalidLastName.clear();
	signup.SignupvalidLastName.sendKeys(LastName);
	Action.waitElementByWebElement(driver, signup.SignupvalidLastName, 20);
	Log.info("Entered Valid New User Last Name");
	
	Assert.assertTrue(signup.SignupvalidEmailAddress.isDisplayed());
	signup.SignupvalidEmailAddress.clear();
	signup.SignupvalidEmailAddress.sendKeys(Email);
	Action.waitElementByWebElement(driver, signup.SignupvalidEmailAddress, 20);
	Log.info("Entered Valid New User Email");

		
	Assert.assertTrue(signup.SignupvalidPassword.isDisplayed());
	signup.SignupvalidPassword.clear();
	signup.SignupvalidPassword.sendKeys(Password);
	Action.waitElementByWebElement(driver, signup.SignupvalidPassword, 20);
	Log.info("Entered Valid New User Password");
	
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Log.info("Click Action Performed on Signup Continue Button.");
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	
	Assert.assertEquals("Organization details below", "Organization details below");
	Log.info("Verfied Next Page info message Text");}

/*---------------------------------------------------------------------------------
	AL-95	Applista > Signup > Check Signup with new user with Invalid conditions
	AL-118  Applista > Signup > Check "CONTINUE" button with Invalid validatio
------------------------------------------------------------------------------------*/
public static void signupInvalidTestCase(WebDriver driver ,String FirstName,String LastName ,String Email,String Password ) throws Exception {
	Log.startTestCase("signup Invalid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(signup.SignupFirstNameText.isDisplayed());
	signup.SignupvalidFirstName.clear();
	signup.SignupvalidFirstName.sendKeys(FirstName);
	Log.info("Entered First Name Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupLastNameText.isDisplayed());
	signup.SignupvalidLastName.clear();
	signup.SignupvalidLastName.sendKeys(LastName);
	Log.info("Entered Last Name Invalid Data for New User");
		 
	Assert.assertTrue(signup.SignupEmailAddressText.isDisplayed());
	signup.SignupEmailAddressField.clear();
	signup.SignupEmailAddressField.sendKeys(Email);
	Log.info("Entered Email Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupPasswordText.isDisplayed());
	signup.SignupPasswordField.clear();
	signup.SignupPasswordField.sendKeys(Password);
	Log.info("Entered Password Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Log.info("Click Action Performed on Signup Continue Button.");
	
	Assert.assertTrue(signup.SignupInvalidFirstNameValidation.isDisplayed());
	Log.info("Signup Invalid First Name Validation Text Verfied");
	Assert.assertTrue(signup.SignupInvalidLastNameValidation.isDisplayed());
	Log.info("Signup Invalid Last Name Validation Text Verfied");
	Assert.assertTrue(signup.SignupInvalidEmailAddressValidation.isDisplayed());
	Log.info("Signup Invalid Email Validation Text Verfied");
	Assert.assertTrue(signup.SignupInvalidPasswordValidation.isDisplayed());
	Log.info("Signup Invalid Password Validation Text Verfied");}
	
/*-------------------------------------------------------------------------
	AL-157 Applista >Logout > Check "Logout" for the account
------------------------------------------------------------------------------------*/
public static void logoutTestCase(WebDriver driver) throws Exception {
//	Log.startTestCase("logout Test Case");
	SignupPage logout = new SignupPage(driver);
	Thread.sleep(1000);
		
	Assert.assertTrue(logout.LogoutClickonUserProfile.isDisplayed());
	Log.info("User Profile Text Verfied");
	
	logout.LogoutClickonUserProfile.isEnabled();
	logout.LogoutClickonUserProfile.click();
	Log.info("Click Action Performed on User Profile Button.");	
	Thread.sleep(2000);
	
	Assert.assertTrue(logout.ClickonLogout.isDisplayed());
	Log.info("Logout Text Verfied");
	
	logout.ClickonLogout.isEnabled();
	logout.ClickonLogout.click();
	Log.info("Click Action Performed on Logout Button.");	
	Action.waitElementByWebElement(driver, logout.ClickonLogout, 20);
	Log.info("Logout Done Succesfully");}

/*------------------------------------------------------------------------------------------------------------------
AL-96  Applista > Signup > Check First name with valid conditions and remaining other fields with invalid conditions
AL-97  Applista > Signup > Check Last name with valid conditions and remaining other fields with invalid conditions
AL-98  Applista > Signup > Check Email with valid conditions and remaining other fields with invalid conditions
AL-99  Applista > Signup > Check Password with valid conditions and remaining other fields with invalid conditions
---------------------------------------------------------------------------------------------------------------------*/
public static void signupwithSingleFieldValidInvalidTestCase(WebDriver driver ,String FirstName,String LastName ,String Email,String Password) throws Exception{
    Log.startTestCase("signup with Single Field Valid Invalid Test Case Started");
    Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
    Assert.assertTrue(signup.SignupFirstNameText.isDisplayed());
    signup.SignupvalidFirstName.clear();
    signup.SignupvalidFirstName.sendKeys(FirstName);
    Action.waitElementByWebElement(driver, signup.SignupvalidFirstName, 20);
    Log.info("Entered Single Filed First Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupLastNameText.isDisplayed());
	signup.SignupvalidLastName.clear();
	signup.SignupvalidLastName.sendKeys(LastName);
	Action.waitElementByWebElement(driver, signup.SignupvalidLastName, 20);
	Log.info("Entered Single Filed Last Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupEmailAddressText.isDisplayed());
	signup.SignupEmailAddressField.clear();
	signup.SignupEmailAddressField.sendKeys(Email);
	Action.waitElementByWebElement(driver, signup.SignupEmailAddressField, 20);
	Log.info("Entered Single Filed Email with Valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupPasswordText.isDisplayed());
	signup.SignupPasswordField.clear();
	signup.SignupPasswordField.sendKeys(Password);
	Action.waitElementByWebElement(driver, signup.SignupPasswordField, 20);
	Log.info("Entered Single Filed Password with valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());	
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	Log.info("Click Action Performed on Signup Continue Button.");
	
	boolean FirstNameIsValid =false,LastNameIsValid= false,EmailAdressIsValid=false,PasswordIsValid=false;
		
	FirstNameIsValid = (FirstName.length()>=3 && FirstName.length()<=20) ? true : false;
	LastNameIsValid = (LastName.length()>=1 && LastName.length()<=20) ? true : false;
	EmailAdressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
	PasswordIsValid = (Password.length()>=8 && Password.length()<=16 ) ? true : false;
		
 if(!FirstNameIsValid){
	Assert.assertTrue(signup.SignupInvalidFirstNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidFirstNameValidation, 20);
	Log.info("Verfied Signup Valid or Invalid First Name Validation");}
   
 if(!LastNameIsValid){
	Assert.assertTrue(signup.SignupInvalidLastNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidLastNameValidation, 20);
 	Log.info("Verfied Signup Valid or Invalid Last Name Validation");}

 if(!EmailAdressIsValid){
	Assert.assertTrue(signup.SignupInvalidEmailAddressValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidEmailAddressValidation, 20);
    Log.info("Verfied Signup Valid or Invalid Email Address Validation");}
    
 if(!PasswordIsValid){
	Assert.assertTrue(signup.SignupInvalidPasswordValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidPasswordValidation, 20);
    Log.info("Verfied Signup Valid or Invalid Password Validation");}}
 
/*-----------------------------------------------------------------------------------------------------------------------------------
	AL-100 Applista > Signup > Check First Name and Last Name with valid conditions and remaining other fields with invalid conditions
	AL-101 Applista > Signup > Check First Name and Email with valid conditions and remaining other fields with invalid conditions
	AL-103 Applista > Signup > Check First Name and Password with valid conditions and remaining other fields with invalid conditions
	AL-104 Applista > Signup > Check Last Name and Password with valid conditions and remaining other fields with invalid conditions
	AL-105 Applista > Signup > Check Last Name and Email with valid conditions and remaining other fields with invalid conditions
	AL-106 Applista > Signup > Check Email and Password with valid conditions and remaining other fields with invalid conditions
------------------------------------------------------------------------------------------------------------------------------------*/
public static void signupwithTwoFieldValidInvalidTestCase(WebDriver driver ,String FirstName,String LastName ,String Email,String Password) throws Exception{
	Log.startTestCase("signup with Two Field Valid Invalid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
    Assert.assertTrue(signup.SignupFirstNameText.isDisplayed());
    signup.SignupvalidFirstName.clear();
    signup.SignupvalidFirstName.sendKeys(FirstName);
	Action.waitElementByWebElement(driver, signup.SignupvalidFirstName, 20);
    Log.info("Entered Two Filed First Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupLastNameText.isDisplayed());
	signup.SignupvalidLastName.clear();
	signup.SignupvalidLastName.sendKeys(LastName);
	Action.waitElementByWebElement(driver, signup.SignupvalidLastName, 20);
	Log.info("Entered Two Filed Last Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupEmailAddressText.isDisplayed());
	signup.SignupEmailAddressField.clear();
	signup.SignupEmailAddressField.sendKeys(Email);
	Action.waitElementByWebElement(driver, signup.SignupEmailAddressField, 20);
	Log.info("Entered Two Filed Email with Valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupPasswordText.isDisplayed());
	signup.SignupPasswordField.clear();
	signup.SignupPasswordField.sendKeys(Password);
	Action.waitElementByWebElement(driver, signup.SignupPasswordField, 20);
	Log.info("Entered Two Filed Password with valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());	
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	Log.info("Click Action Performed on Signup Continue Button.");
	
	boolean FirstNameIsValid =false,LastNameIsValid= false,EmailAdressIsValid=false,PasswordIsValid=false;
		
	FirstNameIsValid = (FirstName.length()>=3 && FirstName.length()<=20) ? true : false;
	LastNameIsValid = (LastName.length()>=1 && LastName.length()<=20) ? true : false;
	EmailAdressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
	PasswordIsValid = (Password.length()>=8 && Password.length()<=16 ) ? true : false;
		
 if(!FirstNameIsValid){
	Assert.assertTrue(signup.SignupInvalidFirstNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidFirstNameValidation, 20);
    Log.info("Verfied Entered Two Filed First Name Is Valid or Invalid");}
	    
 if(!LastNameIsValid){
	Assert.assertTrue(signup.SignupInvalidLastNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidLastNameValidation, 20);
    Log.info("Verfied Entered Two Filed Last Name Is Valid or Invalid");}

 if(!EmailAdressIsValid){
	Assert.assertTrue(signup.SignupInvalidEmailAddressValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidEmailAddressValidation, 20);
    Log.info("Verfied Entered Two Filed Email Is Valid or Invalid");}
    
 if(!PasswordIsValid){
	Assert.assertTrue(signup.SignupInvalidPasswordValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidEmailAddressValidation, 20);
    Log.info("Verfied Entered Two Filed Password Is Valid or Invalid");}}
 
/*--------------------------------------------------------------------------------------------------------------------------------------------
	AL-107 Applista > Signup > Check First Name,Last Name and Email with valid conditions and remaining other fields with invalid conditions
	AL-108 Applista > Signup > Check First Name,Last Name and Password with valid conditions and remaining other fields with invalid conditions
	AL-109 Applista > Signup > Check Last Name,Email and Password with valid conditions and remaining other fields with invalid conditions
	AL-110 Applista > Signup > Check First Name,Email and Password with valid conditions and remaining other fields with invalid conditions
------------------------------------------------------------------------------------------------------------------------------------*/
public static void signupwithThreeFieldValidInvalidTestCase(WebDriver driver ,String FirstName,String LastName ,String Email,String Password) throws Exception{
	Log.startTestCase("signup with Three Field Valid Invalid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
    Assert.assertTrue(signup.SignupFirstNameText.isDisplayed());
    signup.SignupvalidFirstName.clear();
    signup.SignupvalidFirstName.sendKeys(FirstName);
	Action.waitElementByWebElement(driver, signup.SignupvalidFirstName, 20);
    Log.info("Entered Three Filed First Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupLastNameText.isDisplayed());
	signup.SignupvalidLastName.clear();
	signup.SignupvalidLastName.sendKeys(LastName);
	Action.waitElementByWebElement(driver, signup.SignupvalidLastName, 20);
	Log.info("Entered Three Filed Last Name with Valid or Invalid Data for New User");
	 
	Assert.assertTrue(signup.SignupEmailAddressText.isDisplayed());
	signup.SignupEmailAddressField.clear();
	signup.SignupEmailAddressField.sendKeys(Email);
	Action.waitElementByWebElement(driver, signup.SignupEmailAddressField, 20);
	Log.info("Entered Three Filed Email with Valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupPasswordText.isDisplayed());
	signup.SignupPasswordField.clear();
	signup.SignupPasswordField.sendKeys(Password);
	Action.waitElementByWebElement(driver, signup.SignupPasswordField, 20);
	Log.info("Entered Three Filed Password with valid or Invalid Data for New User");
	
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());	
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	Log.info("Click Action Performed on Signup Continue Button.");
	
	boolean FirstNameIsValid =false,LastNameIsValid= false,EmailAdressIsValid=false,PasswordIsValid=false;
		
	FirstNameIsValid = (FirstName.length()>=3 && FirstName.length()<=20) ? true : false;
	LastNameIsValid = (LastName.length()>=1 && LastName.length()<=20) ? true : false;
	EmailAdressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
	PasswordIsValid = (Password.length()>=8 && Password.length()<=16 ) ? true : false;
		
 if(!FirstNameIsValid){
	Assert.assertTrue(signup.SignupInvalidFirstNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidFirstNameValidation, 20);
    Log.info("Verfied Entered Three Filed First Name Is Valid or Invalid");}
	    
 if(!LastNameIsValid){
	Assert.assertTrue(signup.SignupInvalidLastNameValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidLastNameValidation, 20);
    Log.info("Verfied Entered Three Filed Last Name Is Valid or Invalid");}

 if(!EmailAdressIsValid){
	Assert.assertTrue(signup.SignupInvalidEmailAddressValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidEmailAddressValidation, 20);
    Log.info("Verfied Entered Three Filed Email Is Valid or Invalid");}
    
 if(!PasswordIsValid){
	Assert.assertTrue(signup.SignupInvalidPasswordValidation.isDisplayed());
	Action.waitElementByWebElement(driver, signup.SignupInvalidPasswordValidation, 20);
    Log.info("Verfied Entered Three Filed Password Is Valid or Invalid");}}

/*------------------------------------------------------------------------------
	AL-116 Applista > Signup > Password/Confirm password > Check Eye Icon
--------------------------------------------------------------------------------*/
public static void signupPasswordEyeIconTestCase(WebDriver driver) throws Exception {	
	Log.startTestCase("Signup Password Eye Icon Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	SignupPage signup = new SignupPage(driver);
	
	Action.mouseHoverandClick(driver);
	
	signup.SignupvalidFirstName.sendKeys("Sunil");
	Log.info("Entered Valid New User First Name");
	
	signup.SignupvalidLastName.sendKeys("Kumar");
	Log.info("Entered Valid New User Last Name");

	signup.SignupvalidEmailAddress.sendKeys("sunilkumar@mailinator.com");
	Log.info("Entered Valid New User Email");
		
	signup.SignupvalidPassword.sendKeys("password");
	Log.info("Entered Valid New User Password");
	
//	Eye Icon Visibility by default is in disable mode 
    Assert.assertTrue(signup.SignupEyeIconVisibilityOff.isDisplayed());
    
    signup.SignupEyeIconVisibilityOff.click();
    Log.info("Click Action Performed on Visibility Off Password Eye Icon.");
    
//	Click on Eye Icon Visibility or text entered in password field will be visual 
    Assert.assertTrue(signup.SignupEyeIconVisibilityOn.isDisplayed());
    signup.SignupPasswordField.isDisplayed();
	Log.info("Entered password text is Visible");
	
	signup.SignupEyeIconVisibilityOn.click();
	Log.info("Click Action Performed on Visibility On Password Eye Icon.");
	Log.info("Entered password text is not Visible");}
}