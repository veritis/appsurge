package com.applista.qa.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.applista.qa.pages.LoginPage;
import com.applista.qa.pages.OrganizationPage;
import com.applista.qa.pages.SignupPage;
import com.applista.qa.utilities.Log;

public class OrganizationAction {
	
/*----------------------------------------------------------------------------------------------
	AL-119 Applista > Signup > Check Organisation page
	Applista > Signup > Check Organisation page Title 
------------------------------------------------------------------------------------------------*/
public static void organizationPageandTitleTestCase(WebDriver driver ) throws Exception{	
	Log.startTestCase("Organization Page Title Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Organization"));
	Log.info("Organisation page Title Verfied");
	
	OrganizationPage organization = new OrganizationPage(driver);
	
	Assert.assertTrue(organization.SignupUserNameText.isDisplayed());
	Log.info("Signup User Name Text Verfied");
	Assert.assertTrue(organization.OrganizationInfoMessageText.isDisplayed());
	Log.info("Organization Info Message Text Verfied");
	Assert.assertTrue(organization.ApplistaLogoIcon.isDisplayed());
	Log.info("Applista Logo Icon Verfied");
	Assert.assertTrue(organization.OrganizationNameText.isDisplayed());
	Log.info("Organization Name Text Verfied");
	Assert.assertTrue(organization.OrganizationNameField.isDisplayed());
	Log.info("Organization Name Field Verfied");
	Assert.assertTrue(organization.OrganizationWebsiteURLText.isDisplayed());
	Log.info("Organization Website URL Text Verfied");
	Assert.assertTrue(organization.OrganizationWebsiteURLFieldandPlaceHolder.isDisplayed());
	Log.info("Organization Website URL Field and PlaceHolder Verfied");
	Assert.assertTrue(organization.OrganizationYoureDoneButton.isDisplayed());
	Log.info("Organization Youre Done Button Verfied");	
	Assert.assertTrue(organization.OrganizationInvalidNameValidationText.isDisplayed());
	Log.info("Organization Invalid Name Validation Text Verfied");	
	Assert.assertTrue(organization.OrganizationInvalidWebsiteURLValidationText.isDisplayed());
	Log.info("Organization Invalid Website URL Validation Text Verfied");
	Thread.sleep(3000);}

/*----------------------------------------------------------------------------------------------
AL-120 Applista > Signup > Check Organization Name and Website with valid conditions
AL-125 Applista > Signup > Organisation > Check "YOU'RE DONE" button with valid validation
------------------------------------------------------------------------------------------------*/
public static void organizationValidTestCase(WebDriver driver,String FirstName,String LastName,String Email,String Password,String OrganizationName,String OrganizationWebsiteURL ) throws Exception {	
	
	Log.startTestCase("Organization Valid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	OrganizationPage organization = new OrganizationPage(driver);
    LoginPage login = new LoginPage(driver);
    SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(login.LoginDonthaveanaccountText.isDisplayed());
	Assert.assertTrue(login.LoginCreateAccountLink.isDisplayed());
	login.LoginCreateAccountLink.isEnabled();
//	Log.info("Login Create Account Link Verfied");
	Action.mouseHoverandClick(driver);
	signup.SignupvalidFirstName.sendKeys(FirstName);
	Action.waitElementByWebElement(driver, signup.SignupvalidFirstName, 20);
	Log.info("Entered Valid New User First Name");

	signup.SignupvalidLastName.sendKeys(LastName);
	Action.waitElementByWebElement(driver, signup.SignupvalidLastName, 20);
	Log.info("Entered Valid New User Last Name");

	signup.SignupvalidEmailAddress.sendKeys(Email);
	Action.waitElementByWebElement(driver, signup.SignupvalidEmailAddress, 20);
	Log.info("Entered Valid New User Email");

	signup.SignupvalidPassword.sendKeys(Password);
	Action.waitElementByWebElement(driver, signup.SignupvalidPassword, 20);
	Log.info("Entered Valid New User Password");

	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	Log.info("Click Action Performed on Signup Continue Button.");
	
	Thread.sleep(2000);
	Assert.assertTrue(organization.OrganizationNameText.isDisplayed());
	organization.OrganizationNameField.clear();
	organization.OrganizationNameField.sendKeys(OrganizationName);
	Action.waitElementByWebElement(driver, organization.OrganizationNameField, 20);
	Log.info("Entered Valid Organization Name");

	Assert.assertTrue(organization.OrganizationWebsiteURLText.isDisplayed());
	organization.OrganizationWebsiteURLFieldandPlaceHolder.clear();
	organization.OrganizationWebsiteURLFieldandPlaceHolder.sendKeys(OrganizationWebsiteURL);
	Action.waitElementByWebElement(driver, organization.OrganizationWebsiteURLFieldandPlaceHolder, 20);
	Log.info("Entered Valid Organization Website URL");

	Assert.assertTrue(organization.OrganizationYoureDoneButton.isDisplayed());
	Assert.assertTrue(organization.OrganizationYoureDoneButton.isEnabled());
	organization.OrganizationYoureDoneButton.click();
	Action.waitElementByWebElement(driver, organization.OrganizationYoureDoneButton, 20);
	Log.info("Click Action Performed on Organization Youre Done Button.");
	
	Assert.assertEquals("OVERVIEW", "OVERVIEW");
	Log.info("Verfied Next Page Header Text");}

/*----------------------------------------------------------------------------------------------
AL-121 Applista > Signup > Check Organisation Name and Website with Invalid conditions
AL-126 Applista > Signup > Organisation > Check "YOU'RE DONE" button with Invalid validations
AL-122 Applista > Signup > Check Organisation Name with valid conditions and Website with Invalid conditions
AL-124 Applista > Signup > Check Organisation Name with Invalid conditions and Website with Valid conditions
------------------------------------------------------------------------------------------------*/
public static void organizationInvalidTestCase(WebDriver driver,String FirstName,String LastName,String Email,String Password,String OrganizationName,String OrganizationWebsiteURL ) throws Exception {	
	Log.startTestCase("Organization Invalid Test Case Started");
	Log.info("Applista Application Sucessfully Launched ");
	OrganizationPage organization = new OrganizationPage(driver);
    LoginPage login = new LoginPage(driver);
    SignupPage signup = new SignupPage(driver);
	
	Assert.assertTrue(login.LoginDonthaveanaccountText.isDisplayed());
	Assert.assertTrue(login.LoginCreateAccountLink.isDisplayed());
	login.LoginCreateAccountLink.isEnabled();
//	Log.info("Login Create Account Link Verfied");
	Action.mouseHoverandClick(driver);
	signup.SignupvalidFirstName.sendKeys(FirstName);
	Log.info("Entered Valid New User First Name");
	Thread.sleep(1000);
	signup.SignupvalidLastName.sendKeys(LastName);
	Log.info("Entered Valid New User Last Name");
	Thread.sleep(1000);
	signup.SignupvalidEmailAddress.sendKeys(Email);
	Log.info("Entered Valid New User Email");
	Thread.sleep(1000);
	signup.SignupvalidPassword.sendKeys(Password);
	Log.info("Entered Valid New User Password");
	Thread.sleep(1000);
	Assert.assertTrue(signup.SignupContinueButton.isDisplayed());
	Assert.assertTrue(signup.SignupContinueButton.isEnabled());
	signup.SignupContinueButton.click();
	Action.waitElementByWebElement(driver, signup.SignupContinueButton, 20);
	Log.info("Click Action Performed on Signup Continue Button.");

	Assert.assertTrue(organization.OrganizationNameText.isDisplayed());
	organization.OrganizationNameField.clear();
	organization.OrganizationNameField.sendKeys(OrganizationName);
	Action.waitElementByWebElement(driver,organization.OrganizationNameField, 20);
	Log.info("Entered Invalid Organization Name");

	Assert.assertTrue(organization.OrganizationWebsiteURLText.isDisplayed());
	organization.OrganizationWebsiteURLFieldandPlaceHolder.clear();
	organization.OrganizationWebsiteURLFieldandPlaceHolder.sendKeys(OrganizationWebsiteURL);
	Action.waitElementByWebElement(driver,organization.OrganizationWebsiteURLFieldandPlaceHolder, 20);
	Log.info("Entered Invalid Organization Website URL");

	Assert.assertTrue(organization.OrganizationYoureDoneButton.isDisplayed());
	Assert.assertTrue(organization.OrganizationYoureDoneButton.isEnabled());
	organization.OrganizationYoureDoneButton.click();
	Action.waitElementByWebElement(driver,organization.OrganizationYoureDoneButton, 20);
	Log.info("Click Action Performed on Organization Youre Done Button.");
	
	Assert.assertTrue(organization.OrganizationInvalidNameValidationText.isDisplayed());
	Log.info("Organization Invalid Name Validation Text Verfied");
	Assert.assertTrue(organization.OrganizationInvalidWebsiteURLValidationText.isDisplayed());
	Log.info("Organization Invalid WebsiteURL Validation Text Verfied");}
}
