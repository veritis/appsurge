package com.applista.qa.testcases.login;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.applista.qa.actions.LoginAction;
import com.applista.qa.helpers.BaseClass;
import com.applista.qa.utilities.Log;

public class Login {
	
	public WebDriver driver;
	
@BeforeMethod
public void applistaBrowserLanuched() throws Exception {
	driver = BaseClass.Setup();}

@Test(priority = 0) 
public void verifyLoginHomePage() throws Exception {
	 LoginAction.loginHomePageTestCase(driver);
	 Log.info("Login Home Page Sucessfully Verified");}
	  
//@Test(priority = 1) 
//public void verifyFevicon() throws Exception {
//	 LoginAction.loginFeviconTestCase(driver); LoginAction.logoutTestCase(driver);
//	 Log.info("Application Fevicon Sucessfully Verified");}
	 	 
@Test(priority = 2)
public void verifyLoginHomePageTitle() throws Exception {
	 LoginAction.loginPageTitleTestCase(driver);
	 Log.info("Login Home Page Title Sucessfully Verified");}

//@Test(priority =3, dataProvider = "UserValidEmailandPassword" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyUserLoginwithValidData(String Uname ,String Pswd) throws Exception {
//	 LoginAction.loginValidTestCase(driver,Uname,Pswd);
//	 LoginAction.logoutTestCase(driver);
//	 Log.info("Login Account Verified Sucessfully with Valid Email & Password");}
	 
//@Test(priority =4, dataProvider = "UserInvalidEmailandPassword" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyUserLoginwithInvalidData(String Uname ,String Pswd) throws Exception {
//	 LoginAction.loginInvalidTestCase(driver,Uname,Pswd);
//	 Log.info("Login Account Verified Sucessfully with Invalid Email & Password");}
	  
//@Test(priority =5, dataProvider = "UserLoginwithValidorInvalidTestData" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyUserLoginwithValidInvalidData(String Uname ,String Pswd) throws Exception {
//	 LoginAction.userLoginwithValidInvalidDataTestCase(driver,Uname,Pswd);
//	 Log.info("Login Account Verified Sucessfully with Invalid Email & Password");}
	  
//@Test(priority =6) 
//public void verifyLoginPasswordEyeIcon() throws Exception{ 
//	 LoginAction.loginPasswordEyeIconTestCase(driver);
//	 Log.info("Login Password Eye Icon Verfied with Text Visibility Off and On");}
	  
//@Test(priority =7) 
//public void verifyForgotPasswordPage() throws Exception {
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordPageTestCase(driver);
//	 Log.info("Forgot Password Page Sucessfully Verified");} 
	 
//@Test(priority =8) 
//public void verifyForgotPasswordPageTitle() throws Exception { 
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordPageTitleTestCase(driver);
//	 Log.info("Forgot Password Page Title Sucessfully Verified");}
	
//@Test(priority =9,dataProvider = "forgotPasswordValidEmailTestCase" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyForgotPasswordValidEmailTestCase(String Email) throws Exception {
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordValidEmailTestCase(driver,Email);
//	 Log.info("Forgot Password Verified Sucessfully with Valid Email Address");}
	  
//@Test(priority =10,dataProvider = "forgotPasswordInvalidEmailTestCase" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyForgotPasswordInvalidEmailTestCase(String Email) throws Exception {
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordInvalidEmailTestCase(driver,Email);
//	 Log.info("Forgot Password Verified Sucessfully with Invalid Email Address");}
	  
//@Test(priority =11) 
//public void verifyLoginPasswordMailBody() throws Exception{ 
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordResetPasswordMailBodyTestCase(driver);
//	 Log.info("Forgot Password Reset Password Mail Link Verfied Sucessfully ");}
	  
//@Test(priority =12 ) 
//public void verifyResetPasswordPage() throws Exception {
//	 LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordResetPasswordMailBodyTestCase(driver);
//	 LoginAction.resetPasswordPageTestCase(driver);
//	 Log.info("Reset Password Page Sucessfully Verified");}
	  
//@Test(priority =13,dataProvider = "ResetPasswordValidTestData" ,dataProviderClass = LoginDataProvider.class) 
//public void verifyChangePasswordValidTestCase(String NewPassword,String ConfirmPassword)throws Exception { LoginAction.forgotPasswordLinkTestCase(driver);
//	 LoginAction.forgotPasswordResetPasswordMailBodyTestCase(driver);
//	 LoginAction.changePasswordValidTestCase(driver, NewPassword,ConfirmPassword);
//	 Log.info("Reset Password Sucessfully Changed with Valid Test Data");}
	 
//@Test(priority =1,dataProvider = "ResetPasswordInvalidTestData" , dataProviderClass = LoginDataProvider.class)
//public void verifyChangePasswordInvalidTestCase(String NewPassword,String ConfirmPassword) throws Exception {
//	LoginAction.forgotPasswordLinkTestCase(driver);
//	LoginAction.forgotPasswordResetPasswordMailBodyTestCase(driver);
//	LoginAction.changePasswordInvalidTestCase(driver, NewPassword, ConfirmPassword);
//	Log.info("Reset Password Sucessfully Changed with Valid Test Data");}

@AfterMethod
public void teardownbrowser() throws Exception {
	 driver.quit();
	 Log.info("Applista Application Succefly Closed");
	 Log.endTestCase("Test Case End");}
}
