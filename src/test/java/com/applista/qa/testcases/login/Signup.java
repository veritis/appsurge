package com.applista.qa.testcases.login;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.applista.qa.actions.LoginAction;
import com.applista.qa.actions.SignupAction;
import com.applista.qa.helpers.BaseClass;
import com.applista.qa.testdata.LoginDataProvider;
import com.applista.qa.utilities.Log;

public class Signup {

	public WebDriver driver;
  	
@BeforeMethod
public void applistaBrowserLanuched() throws Exception {
	driver = BaseClass.Setup();}

@Test(priority =1)
public void verifySignupPage() throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupPageTestCase(driver);
	Log.info("Signup Page Succesfully verified");}

@Test(priority =2)
public void verifySignupPageTitle() throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupPageTitleTestCase(driver);
	Log.info("Signup Page Title Succesfully verified with valid & Invalid URL");}

@Test(priority =3,dataProvider = "UserSignupwithValidTestdata" , dataProviderClass = LoginDataProvider.class)
public void verifyUserSignupwithValidData(String FirstName,String LastName,String Email,String Password) throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupValidTestCase(driver,FirstName,LastName,Email,Password);
	SignupAction.logoutTestCase(driver);
	Log.info("Succesfully Verified Signup for new user with Valid Data");}

@Test(priority =4, dataProvider = "UserSignupwithInvalidTestdata" , dataProviderClass = LoginDataProvider.class)
public void verifyUserSignupwithInvalidData(String FirstName,String LastName,String Email,String Password) throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupInvalidTestCase(driver,FirstName,LastName,Email,Password);
	Log.info("Succesfully Verified Signup for new user with Invalid Data");}

@Test(priority =5, dataProvider = "SignupValid&InvalidSingleFieldTestData" , dataProviderClass = LoginDataProvider.class)
public void verifySignupSingleFieldwithValidInvalidData(String FirstName,String LastName,String Email,String Password) throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupwithSingleFieldValidInvalidTestCase(driver,FirstName,LastName,Email,Password);
	Log.info("Succesfully Verfied Signup Single Field FirstName ,LastName,Email,Password with Valid & Invalid Data");}

@Test(priority =6, dataProvider = "SignupValid&InvalidTwoFieldsTestData" , dataProviderClass = LoginDataProvider.class)
public void verifySignuptwoFieldswithValidInvalidData(String FirstName,String LastName,String Email,String Password) throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupwithTwoFieldValidInvalidTestCase(driver,FirstName,LastName,Email,Password);
	Log.info("Succesfully Verfied Signup Two Field FirstName ,LastName,Email,Password with Valid & Invalid Data");}

@Test(priority =7, dataProvider = "SignupValid&InvalidSThreeFieldsTestData" , dataProviderClass = LoginDataProvider.class)
public void verifySignupThreeFieldswithValidInvalidData(String FirstName,String LastName,String Email,String Password) throws Exception {
	LoginAction.loginCreateAccountLinkTestCase(driver);
	SignupAction.signupwithThreeFieldValidInvalidTestCase(driver,FirstName,LastName,Email,Password);
	Log.info("Succesfully Verfied Signup Three Field FirstName ,LastName,Email,Password with Valid & Invalid Data");}

@Test(priority =8)
public void verifySignupPasswordEyeIcon() throws Exception {
	SignupAction.signupPasswordEyeIconTestCase(driver);
	Log.info("Signup Password Eye Icon Verfied with Text Visibility Off and On");}

@AfterMethod
public void teardownbrowser() throws Exception {
	driver.quit();
	Log.info("Applista Application Successfully Closed");
	Log.endTestCase("Test Case End");}
}
