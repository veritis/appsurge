package com.applista.qa.testcases.login;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.applista.qa.actions.OrganizationAction;
import com.applista.qa.actions.SignupAction;
import com.applista.qa.helpers.BaseClass;
import com.applista.qa.testdata.LoginDataProvider;
import com.applista.qa.utilities.Log;

public class Organization {
	public WebDriver driver;
  	
@BeforeMethod
public void applistaBrowserLanuched() throws Exception {
	driver = BaseClass.Setup();}

@Test(priority =0)
public void verifyOrganizationPageandTitleTestCase() throws Exception {
	OrganizationAction.organizationPageandTitleTestCase(driver);
	SignupAction.logoutTestCase(driver);
	Log.info("Organization Succesfully verified for new user with Valid Data");}

@Test(priority = 1 ,dataProvider = "OrganizationValidTestData" , dataProviderClass = LoginDataProvider.class)
public void verifyOrganizationValidDataTestCase(String FirstName,String LastName,String Email,String Password,String OrganizationName,String OrganizationWebsiteURL) throws Exception {
	OrganizationAction.organizationValidTestCase(driver,FirstName,LastName,Email,Password,OrganizationName,OrganizationWebsiteURL);
	SignupAction.logoutTestCase(driver);
	Log.info("Organization Succesfully verified for new user with Valid Data");}

//@Test(priority = 2 ,dataProvider = "OrganizationInvalidTestData" , dataProviderClass = LoginDataProvider.class)
//public void verifyOrganizationInvalidDataTestCase(String FirstName,String LastName,String Email,String Password,String OrganizationName,String OrganizationWebsiteURL) throws Exception {
//	OrganizationAction.organizationInvalidTestCase(driver,FirstName,LastName,Email,Password,OrganizationName,OrganizationWebsiteURL);
//	SignupAction.logoutTestCase(driver);
//	Log.info("Organization Succesfully verified for new user with Invalid Data");}

@AfterMethod
public void teardownbrowser() throws Exception {
	driver.quit();
	Log.info("Applista Application Successfully Closed");
	Log.endTestCase("Test Case End");}
}
